const express = require("express");
const path = require("path");
const app = express();
const bodyParser = require("body-parser");
const axios = require('axios');

const port = process.env.PORT || 5000;

app.use(bodyParser.json());

/**
 *
 */
app.post("/json", (req, res) => {
  console.log("request:", req.body);

  let user = {
    login: "default",
    password: "default"
  };

  user.login = req.body.login;
  user.password = req.body.password;

  console.log(user);

  axios
    .post("https://node-react-first.firebaseio.com/users.json", user)
    .then(response => {
      console.log(response.data)
    })
    .catch(error => {
    console.log(error)
    });

  // console.log('response:',res);
});

// API calls
app.get("/api/hello", (req, res) => {
  res.send({ express: "Hello From Express" });
});

if (process.env.NODE_ENV === "production") {
  // Serve any static files
  app.use(express.static(path.join(__dirname, "client/build")));
  // Handle React routing, return all requests to React app
  app.get("*", function(req, res) {
    res.sendFile(path.join(__dirname, "client/build", "index.html"));
  });
}

app.listen(port, () => console.log(`Listening on port ${port}`));
