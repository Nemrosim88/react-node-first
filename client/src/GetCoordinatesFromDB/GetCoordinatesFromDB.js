import React, { Component } from "react";
import axios from "axios";

class GetCoordinatesFromDB extends Component {
  /**
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);
    this.state = {
      login: "noLogin",
      password: "noPassword"
    };

    this.handleGetCoordinatesFromDB = this.handleGetCoordinatesFromDB.bind(
      this
    );
  }

  /**
   *
   * @param {*} event
   */
  handleGetCoordinatesFromDB(event) {
    event.preventDefault();
    axios
      .get("https://node-react-first.firebaseio.com/coordinates.json")
      .then(response => {
        console.log(response);
      })
      .catch(error => {});
  }

  /**
   *
   */
  render() {
    return (
      <form onSubmit={this.handleGetCoordinatesFromDB}>
        <button type="submit" className="btn btn-primary">
          Get all coordinates
        </button>
      </form>
    );
  }
}

export default GetCoordinatesFromDB;
