import React, { Component } from "react";
import "../lib/mapquest-core";
import "../lib/mapquest-core.css";

class Map extends Component {
  constructor(props) {
    super(props);
    this.state = { ...props };
  }

  render() {
    return (
      <div id="map" styleName="width: 100%; height: 530px;">
        <p>Map is loading...</p>
      </div>
    );
  }

  componentDidMount() {
    L.mapquest.key = this.state.apiKey;

    // 'map' refers to a <div> element with the ID map
    L.mapquest.map("map", {
      center: [37.7749, -122.4194],
      layers: L.mapquest.tileLayer("map"),
      zoom: 12
    });
  }
}

export default Map;
