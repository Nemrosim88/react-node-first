import React, { Component } from "react";
import axios from "axios";

class LoginForm extends Component {
  /**
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);
    this.state = {
      login: "noLogin",
      password: "noPassword"
    };

    this.handleLoginChange = this.handleLoginChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
   *
   * @param {*} event
   */
  handleLoginChange(event) {
    this.setState({ login: event.target.value });
  }

  /**
   *
   * @param {*} event
   */
  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }

  /**
   *
   * @param {*} event
   */
  handleSubmit(event) {
    // alert("An essay was submitted: " + this.state.value);
    event.preventDefault();
    axios
      .post("/json", this.state)
      .then(response => {})
      .catch(error => {});
  }

  /**
   *
   */
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlFor="exampleInputEmail1">Login</label>
          <input
            type="text"
            className="form-control"
            id="exampleInputLogin1"
            aria-describedby="emailHelp"
            placeholder="Enter login"
            onChange={this.handleLoginChange}
          />
          <small id="emailHelp" className="form-text text-muted">
            We'll never share your email with anyone else.
          </small>
        </div>
        <div className="form-group">
          <label htmlFor="exampleInputPassword1">Password</label>
          <input
            type="password"
            className="form-control"
            id="exampleInputPassword1"
            placeholder="Password"
            onChange={this.handlePasswordChange}
          />
        </div>
        <div className="form-group form-check">
          <input
            type="checkbox"
            className="form-check-input"
            id="exampleCheck1"
          />
          <label className="form-check-label" htmlFor="exampleCheck1">
            Check me out
          </label>
        </div>

        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    );
  }
}

export default LoginForm;
